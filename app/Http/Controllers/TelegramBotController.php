<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramBotController extends Controller
{
    //
    public function updatedActivity()
    {
        $activity = Telegram::getUpdates();
        dd($activity);
    }

    public function sendMessage()
    {
        return view('message');
    }

    public function storeMessage(Request $request)
    {
        $request->validate([
           
            'message' => 'required'
        ]);

        $text = "<b>Name: </b>\n"
        . "$request->email\n"
        . "<b>Message: </b>\n"
        . $request->message;

        Telegram::sendMessage([
            'chat_id' => env('TELEGRAM_CHANNEL_ID', ''),
            'parse_mode' => 'HTML',
            'text' => $text
        ]);

        return redirect()->back();
    }

    public function sendPhoto()
    {
        return view('photo');
    }

    public function storePhoto(Request $request)
    {
        $request->validate([
            'file' => 'file|mimes:jpeg,png,gif'
        ]);

        $photo = $request->file('file');

        Telegram::sendPhoto([
            'chat_id' => env('TELEGRAM_CHANNEL_ID', ''),
            'photo' => InputFile::createFromContents(file_get_contents($photo->getRealPath()), str_random(10) . '.' . $photo->getClientOriginalExtension()),
            'caption'=>"HELLO",
            'parse_mode'=>"html",
            'disable_web_page_preview'=>false,
            'reply_markup'=>json_encode([
                'inline_keyboard'=>[
                    [['text'=>'laravel','url'=>'https://laravel.com/docs/5.7/installation']],
                    [['text'=>'mysql','url'=>'https://dev.mysql.com/doc/refman/8.0/en/introduction.html']],
                    [['text'=>'php','url'=>'http://php.net/manual/en/index.php']],
                ]
            ])
        ]);

        return redirect()->back();
    }
}
